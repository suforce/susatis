import dotenv from 'dotenv';
import moment from 'moment';
import expressApp from './main';

dotenv.config();
const server = expressApp.listen();

moment.suppressDeprecationWarnings = true;

function terminate(server: any, options = { coredump: false, timeout: 500 }) {
	const exit = function (code: number) {
		options.coredump ? process.abort() : process.exit(code);
	};

	return (code: number, reason: any) => (err: any, promise: any) => {
		if (err && err instanceof Error) console.log(err.message, err.stack);
		server.close(exit);
		//@ts-ignore
		const timeout = setTimeout(exit, options.timeout).unref();
	};
}

const exitHandler = terminate(server, {
	coredump: false,
	timeout: 500,
});

process.on('uncaughtException', exitHandler(1, 'Unexpected Error'));
process.on('unhandledRejection', exitHandler(1, 'Unhandled Promise'));
process.on('SIGTERM', exitHandler(0, 'SIGTERM'));
process.on('SIGINT', exitHandler(0, 'SIGINT'));
