class SweetAlert {
	/**
	 *
	 * @param {String} mesaj Modal içerisinde görünmesini istediğiniz mesaj
	 * @returns boolean Yeşil butona basılırsa true, kırmızıya basılırsa false döndürür.
	 */
	static async showQuestionAlert(mesaj) {
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success',
				cancelButton: 'btn btn-danger mr-2',
			},
			showClass: { popup: 'animate__animated animate__fadeInDown' },
			hideClass: { popup: 'animate__animated animate__fadeOutUp' },
			buttonsStyling: false,
		});

		const swal = await swalWithBootstrapButtons.fire({
			title: 'Emin Misiniz ?',
			text: mesaj,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Evet sil !',
			cancelButtonText: 'Hayır Vazgeçtim',
			reverseButtons: true,
		});
		return swal.isConfirmed;
	}

	/**
	 *
	 * @param {String} mesaj Modal içerisinde görünmesini istediğiniz mesaj
	 */
	static async showOkAlert(mesaj, icon,title='Başarılı') {
		await Swal.fire({
			title,
			text: mesaj,
			icon,
			showClass: { popup: 'animate__animated animate__fadeInDown' },
			hideClass: { popup: 'animate__animated animate__fadeOutUp' },
		});
	}
}
