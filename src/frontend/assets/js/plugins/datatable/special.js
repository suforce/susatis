class DataTable {
	static datatable(id) {
		const ozellikler = {
			className: 'mr-1',
			exportOptions: { columns: [0, 1, 2, 3, 4] },
			title: 'Ürünler',
		};
		$(id).DataTable({
			dom: 'Bfrtip',
			buttons: [
				{ extend: 'excelHtml5', text: 'Excel Olarak Kaydet', ...ozellikler },
				{ extend: 'pdf', text: 'PDF Olarak Kaydet', ...ozellikler },
			],
			responsive: true,
			language: {
				sProcessing: 'İşleniyor...',
				sLengthMenu: 'Sayfada _MENU_ Kayıt Göster',
				sZeroRecords: 'Eşleşen Kayıt Bulunmadı',
				sInfo: '  _TOTAL_ Kayıttan _START_ - _END_ Arası Kayıt Gösteriliyor.',
				sInfoEmpty: 'Kayıt Yok',
				sInfoFiltered: '(_MAX_ Kayıt İçerisinden Bulunan)',
				sInfoPostFix: '',
				sSearch: 'Bul:',
				sUrl: '',
				oPaginate: {
					sFirst: 'İlk',
					sPrevious: 'Önceki',
					sNext: 'Sonraki',
					sLast: 'Son',
				},
			},
		});
		$('.buttons-excel').removeClass('btn-secondary').addClass('btn-success');
		$('.buttons-pdf').removeClass('btn-secondary').addClass('btn-primary');
		api.get('download', function (sonuc) {
			if (sonuc == 'completed') SweetAlert.showOkAlert('Dosya Başarıyla Kaydedildi !', 'success');
			else if (sonuc != 'cancelled') SweetAlert.showOkAlert('Dosya Kaydedilirken Bir Sorun Oluştu !', 'error');
		});
	}
}
