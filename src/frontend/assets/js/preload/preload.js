const { IpcClient } = require('ipc-express');
const { ipcRenderer, contextBridge } = require('electron');

const ipc = new IpcClient(ipcRenderer);
contextBridge.exposeInMainWorld('api', {
	get: async function (path, data) {
		return await ipc.get(path, data);
	},
	post: async function (path, data) {
		return await ipc.post(path, data);
	},
	delete: async function (path, data) {
		return await ipc.delete(path, data);
	},
	put: async function (path, data) {
		return await ipc.put(path, data);
	},
});
