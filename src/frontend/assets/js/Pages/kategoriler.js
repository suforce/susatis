api.send('kategoriler','');
api.get('kategoriler',function(kategoriler){
    $('#kategoriler tbody').empty();
    kategoriler.forEach(function(kategori){
        document.querySelector('#kategoriler tbody').innerHTML += `
        <tr>
            <td>${kategori.id}</td>
            <td>${kategori.kategoriAdi}</td>
            <td>
                <a href="kategori-duzenle.pug?id=${kategori.id}" class="btn btn-warning">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="javascript:" class="btn btn-danger" onclick="kategoriSil('${urun.id}')">
                    <i class="fa fa-trash"></i>
                </a>
            </td>
        </tr>`;
    });
    if (!$.fn.DataTable.isDataTable('table#kategoriler')) DataTable.datatable('#kategoriler');
});

function kategoriSil(id){
    SweetAlert.showQuestionAlert('Bu Kategoriyi Silmek İstediğinize Emin Misiniz ?').then(function (sonuc) {
		if (sonuc) {
			window.api.send('kategori-sil', id);
			SweetAlert.showOkAlert('Kategori Silindi !', 'success').then(() => location.reload());
		}
	});
}