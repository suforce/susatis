window.api.send('urunler');
window.api.get('urunler', (urunler) => {
	$('#urunler tbody').empty();
	urunler.forEach((urun) => {
		document.querySelector('#urunler tbody').innerHTML += `
        <tr>
            <td>${urun.id}</td>
            <td>${urun.urunAdi}</td>
            <td>${urun.kategoriler.kategoriAdi}</td>
            <td>${urun.urunFiyati}</td>
            <td>${urun.stokSayisi}</td>
            <td>
                <a href="urun-duzenle.pug?id=${urun.id}" class="btn btn-warning">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="javascript:" class="btn btn-danger" onclick="urunSil('${urun.id}')">
                    <i class="fa fa-trash"></i>
                </a>
            </td>
        </tr>`;
	});
	if (!$.fn.DataTable.isDataTable('table#urunler')) DataTable.datatable('#urunler');
});

function urunSil(id) {
	SweetAlert.showQuestionAlert('Bu Ürünü Silmek İstediğinize Emin Misiniz ?').then(function (sonuc) {
		if (sonuc) {
			window.api.send('urun-sil', id);
			SweetAlert.showOkAlert('Ürün Silindi !', 'success').then(() => location.reload());
		}
	});
}
