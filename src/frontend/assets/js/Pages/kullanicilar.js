api.send('kullanicilar');
api.get('kullanicilar', function (kullanicilar) {
	$('#kullanicilar tbody').empty();
	kullanicilar.forEach(function (kullanici) {
		document.querySelector('#kullanicilar tbody').innerHTML += `
        <tr>
            <td>${kullanici.id}</td>
            <td>${kullanici.adSoyad}</td>
            <td>${kullanici.kullaniciAdi}</td>
            <td>${kullanici.email}</td>
            <td>
                <a href="kullanici-duzenle.pug?id=${kullanici.id}" class="btn btn-warning">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="javascript:" class="btn btn-danger" onclick="kullaniciSil('${kullanici.id}')">
                    <i class="fa fa-trash"></i>
                </a>
            </td>
        </tr>`;
	});
	DataTable.datatable('#kullanicilar');
});

function kullaniciSil(id) {
	SweetAlert.showQuestionAlert('Bu Kullanıcıyı Silmek İstediğinize Emin Misiniz ?').then(function (sonuc) {
		if (sonuc) {
			window.api.send('kullanici-sil', id);
			SweetAlert.showOkAlert('Kullanıcı Silindi !', 'success').then(() => location.reload());
		}
	});
}
