const usernameInput = document.querySelector('.username');
const passwordInput = document.querySelector('.password');
const face = document.querySelector('.face');
passwordInput.addEventListener('focus', function (event) {
	document.querySelectorAll('.hand').forEach((hand) => hand.classList.add('hide'));
	document.querySelector('.tongue').classList.remove('breath');
});

passwordInput.addEventListener('blur', function (event) {
	document.querySelectorAll('.hand').forEach(function (hand) {
		hand.classList.remove('hide');
		hand.classList.remove('peek');
	});
	document.querySelector('.tongue').classList.add('breath');
});

usernameInput.addEventListener('focus', function (event) {
	let length = Math.min(usernameInput.value.length - 16, 19);
	document.querySelectorAll('.hand').forEach(function (hand) {
		hand.classList.remove('hide');
		hand.classList.remove('peek');
	});
	face.style.setProperty('--rotate-head', `${-length}deg`);
});

usernameInput.addEventListener('input', function (event) {
	let length = Math.min(event.target.value.length - 16, 19);
	face.style.setProperty('--rotate-head', `${-length}deg`);
});

document.querySelector('.login-button').addEventListener('click', function () {
	const username = usernameInput.value.trim();
	const password = passwordInput.value.trim();
	if (!username || username == null || !password || password == null)
		SweetAlert.showOkAlert('Kullanıcı Adı Yada Şifre Boş Olamaz !', 'error', 'Hata');
	else {
		api.post('/giris-yap', { username, password }).then(function (sonuc) {
			if (!sonuc) SweetAlert.showOkAlert('Kullanıcı Adı Yada Şifre Hatalı, Tekrar Deneyiniz !', 'error', 'Hata');
			else location.href = 'kullanicilar.pug';
		});
	}
});

document.querySelectorAll('input').forEach((input) =>
	input.addEventListener('keypress', function (event) {
		if (event.which == 13) document.querySelector('.login-button').click();
	}),
);

usernameInput.addEventListener('blur', (event) => face.style.setProperty('--rotate-head', '0deg'));
