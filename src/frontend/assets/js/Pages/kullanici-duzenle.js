const id = new URLSearchParams(location.search).get('id');
api.send('kullanicilar', id);
const kullaniciAdiInput = document.getElementById('kullaniciAdi');
const adSoyadInput = document.getElementById('adSoyad');
const emailInput = document.getElementById('email');
const sifreInput = document.getElementById('sifre');

api.get('kullanici', function (kullanici) {
	console.log({ kullanici });
	kullaniciAdiInput.value = kullanici.kullaniciAdi;
	adSoyadInput.value = kullanici.adSoyad;
	emailInput.value = kullanici.email;
	sifreInput.value = kullanici.sifre;
});

document.getElementById('kaydet').addEventListener('click', function () {
	const kullaniciAdi = kullaniciAdiInput.value.trim();
	const adSoyad = adSoyadInput.value.trim();
	const email = emailInput.value.trim();
	const sifre = sifreInput.value.trim();
	if (!kullaniciAdi || !adSoyad || !email || !sifre) SweetAlert.showOkAlert('Boş Alan Bırakmayın !', 'error');
	else {
		api.send('kullanici-duzenle', { kullaniciAdi, adSoyad, email, sifre });
		SweetAlert.showOkAlert('Kullanıcı Bilgileri Başarıyla Güncellendi !', 'success');
	}
});

document.querySelector('form').addEventListener('submit', (e) => e.preventDefault());
