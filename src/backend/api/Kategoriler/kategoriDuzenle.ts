import { Request, Response } from 'express';
import KategorilerApi from '../../services/kategoriler';

export default async function (req: Request, res: Response) {
	res.end(await KategorilerApi.kategoriDuzenle(parseInt(req.params.id), req.body.kategoriAdi));
}
