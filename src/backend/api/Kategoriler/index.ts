import { Router } from 'express';

export default function (app: Router) {
	app.post('/kategoriler', require('./kategoriOlustur').default);
	app.get('/kategoriler/:id', require('./kategoriGetir').default);
	app.delete('/kategoriler/:id', require('./kategoriSil').default);
	app.put('/kategoriler/:id', require('./kategoriDuzenle').default);
	app.get('/kategoriler', require('./tumKategorileriGetir').default);
}
