import { Request, Response } from 'express';
import KategorilerApi from '../../services/kategoriler';

export default async function (req: Request, res: Response) {
	res.end(await KategorilerApi.findById(parseInt(req.params.id)));
}
