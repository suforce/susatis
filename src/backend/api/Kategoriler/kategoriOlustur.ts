import { Request, Response } from 'express';
import KategorilerApi from '../../services/kategoriler';

export default async function (req: Request, res: Response) {
	res.send(await KategorilerApi.kategoriOlustur(req.body.kategoriAdi));
}
