import { Request, Response } from 'express';
import UrunlerApi from '../../services/urunler';

export default async function (req: Request, res: Response) {
	res.end(await UrunlerApi.urunOlustur(req.body.data));
}
