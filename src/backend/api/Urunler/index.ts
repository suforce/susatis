import { Router } from 'express';

export default function (app: Router) {
	app.post('/urunler', require('./urunOlustur').default);
	app.get('/urunler/:id', require('./urunGetir').default);
	app.delete('/urunler/:id', require('./urunSil').default);
	app.put('/urunler/:id', require('./urunDuzenle').default);
	app.get('/urunler', require('./tumUrunleriGetir').default);
}
