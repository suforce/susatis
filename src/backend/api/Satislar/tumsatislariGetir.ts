import { Request, Response } from 'express';
import SatislarApi from '../../services/satislar';

export default async function (req: Request, res: Response) {
	res.end(await SatislarApi.tumSatislariBul());
}
