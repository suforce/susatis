import { Router } from 'express';

export default function (app: Router) {
	app.post('/satislar', require('./satisOlustur').default);
	app.get('/satislar/:id', require('./satisGetir').default); // Satış id'ye göre getir
	app.get('/satis-filtrele/:id', require('./satisFiltrele').default); // Ürün adı ve tarihe göre filtrele
	app.get('/satislar', require('./tumsatislariGetir').default);
}
