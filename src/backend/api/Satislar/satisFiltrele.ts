import { Request, Response } from 'express';
import SatislarApi from '../../services/satislar';

export default async function (req: Request, res: Response) {
	const query = new URLSearchParams(String(req.query));
	res.end(await SatislarApi.satisiBul(parseInt(query.get('urun')!), query.get('tarih')!));
}
