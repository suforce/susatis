import { Request, Response } from 'express';
import SatislarApi from '../../services/satislar';

export default async function (req: Request, res: Response) {
	await SatislarApi.satisYap(req.body.data);
}
