import express from 'express';
import expressApp from '../../main';
// import bodyParser from 'body-parser';

// expressApp.use(bodyParser.json());
const routes = express.Router();

require('./Kategoriler').default(routes);
require('./Urunler').default(routes);
require('./Kullanicilar').default(routes);
require('./Satislar').default(routes);

export default expressApp;
