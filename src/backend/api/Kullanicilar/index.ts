import { Router } from 'express';

export default function (app: Router) {
	app.post('/giris-yap', require('./girisYap').default);
	app.post('/kullanicilar', require('./kullaniciOlustur').default);
	app.get('/kullanicilar/:id', require('./kullaniciGetir').default);
	app.delete('/kullanicilar/:id', require('./kullaniciSil').default);
	app.put('/kullanicilar/:id', require('./kullaniciDuzenle').default);
	app.get('/kullanicilar', require('./tumKullanicilariGetir').default);
}
