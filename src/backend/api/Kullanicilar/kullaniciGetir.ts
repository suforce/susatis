import { Request, Response } from 'express';
import KullanicilarApi from '../../services/kullanicilar';

export default async function (req: Request, res: Response) {
	res.end(await KullanicilarApi.findById(parseInt(req.params.id)));
}
