import fse from 'fs-extra';
import constants from '../constants';

export default class FileOperations {
	private settings: any = {
		dbCreate: false,
	};

	createFiles() {
		if (!fse.existsSync(constants.TEMP_PATH)) fse.mkdirpSync(constants.TEMP_PATH);
		if (!fse.existsSync(constants.SETTINGS_FILE))
			fse.writeFileSync(constants.SETTINGS_FILE, JSON.stringify({ ...this.settings }));
	}

	readSettingsFile() {
		return JSON.parse(fse.readFileSync(constants.SETTINGS_FILE).toString());
	}

	writeSettingsFile(key: string, value: any) {
		const settings = this.readSettingsFile();
		settings[key] = value;
		this.settings[key] = value;
		fse.writeFileSync(constants.SETTINGS_FILE, JSON.stringify(this.settings));
	}
}
