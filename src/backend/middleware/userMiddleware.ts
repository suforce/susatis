import Error401 from '../Errors/error401';
import WildcardMatch from 'wildcard-match';
import KullanicilarApi from '../services/kullanicilar';
import { Request, Response, NextFunction } from 'express';

export default function userMiddleware(req: Request, res: Response, next: NextFunction) {
	const gozArdiEdilecekIstekler = WildcardMatch(['giris-yap']);
	if (gozArdiEdilecekIstekler(req.path)) return next();
	try {
		//@ts-ignore
		req.user = KullanicilarApi.findById(5);
	} catch (error) {
		res.end(new Error401());
	}
}
