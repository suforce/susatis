import { sequelize } from '.';
import { DataTypes, Model } from 'sequelize';

export default class Kullanicilar extends Model {}
Kullanicilar.init(
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		adSoyad: DataTypes.STRING({ length: 255 }),
		kullaniciAdi: DataTypes.STRING({ length: 255 }),
		sifre: DataTypes.STRING({ length: 255 }),
		email: {
			type: DataTypes.STRING({ length: 255 }),
			validate: { isEmail: true },
		},
	},
	{
		sequelize,
		tableName: 'kullanicilar',
		timestamps: false,
		paranoid: false,
	},
);
