import { sequelize } from '.';
import { DataTypes, Model } from 'sequelize';

export default class Kategoriler extends Model {}
Kategoriler.init(
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		kategoriAdi: DataTypes.TEXT,
	},
	{
		sequelize,
		tableName: 'kategoriler',
		timestamps: false,
		paranoid: false,
	},
);