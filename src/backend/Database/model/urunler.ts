import { sequelize } from '.';
import Kategoriler from './kategoriler';
import { DataTypes, Model } from 'sequelize';

export default class Urunler extends Model {}
Urunler.init(
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		urunAdi: DataTypes.TEXT,
		urunFiyati: DataTypes.DECIMAL(10, 2),
		stokSayisi: DataTypes.INTEGER,
	},
	{
		sequelize,
		tableName: 'urunler',
		timestamps: false,
		paranoid: false,
	},
);
Urunler.belongsTo(Kategoriler, { as: 'kategoriler' });