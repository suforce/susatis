import moment from 'moment';
import { sequelize } from '.';
import Urunler from './urunler';
import { Model, DataTypes } from 'sequelize';

export default class Satislar extends Model {}
Satislar.init(
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		urunAdedi: DataTypes.INTEGER,
		satisFiyati: DataTypes.DECIMAL(10, 2),
		satisTarihi: {
			type: DataTypes.DATE,
			get: function () {
				return moment(this.getDataValue('satisTarihi'), 'DD.MM.YYYY HH:mm:ss');
			},
		},
	},
	{
		sequelize,
		tableName: 'satislar',
		timestamps: false,
		paranoid: false,
	},
);
Satislar.belongsTo(Urunler, { as: 'urunler' });
