import Urunler from '../model/urunler';
import Satislar from '../model/satislar';
import Kategoriler from '../model/kategoriler';
import Kullanicilar from '../model/kullanicilar';

async function createDatabase() {
	await Urunler.sync();
	await Satislar.sync();
	await Kategoriler.sync();
	await Kullanicilar.sync();
}

export default createDatabase;