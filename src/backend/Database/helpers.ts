export default class DatabaseHelper {
	static async _fillWithRelationsAndFilesForRows(rows: any) {
		if (!rows) return rows;
		if (Array.isArray(rows)) {
			return Promise.all(
				rows.map((record: any) => {
					const output = record.get({ plain: true });
					return output;
				}),
			);
		} else return rows.get({ plain: true });
	}
}
