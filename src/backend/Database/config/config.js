const os = require('os');
const path = require('path');

require('dotenv').config();

module.exports = {
	dialect: 'sqlite',
	storage:
		os.platform() == 'win32'
			? path.join(os.tmpdir(), '..', process.env.APP_NAME)
			: path.join(os.tmpdir(), process.env.APP_NAME) + '/db.db',
};
