import tr from './tr';
import _get from 'lodash/get';

const languages: any = {
	tr: tr,
};

function format(message: string, args: any) {
	if (!message) return null;
	return message.replace(/{(\d+)}/g, function (match, number) {
		return typeof args[number] != 'undefined' ? args[number] : match;
	});
}

export const i18nExists = (languageCode: string, key: any) => {
	const dictionary = languages[languageCode] || languages['tr'];
	const message = _get(dictionary, key);
	return Boolean(message);
};

export const i18n = (languageCode: string, key: string, ...args: any) => {
	const dictionary = languages[languageCode] || languages['tr'];
	const message = _get(dictionary, key);
	if (!message) return key;
	return format(message, args);
};
