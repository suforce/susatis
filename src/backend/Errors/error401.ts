import { i18n, i18nExists } from '../language';

export default class Error401 extends Error {
	code: Number;

	constructor(language?: any, messageCode?: any, ...args: any) {
		let message;
		if (messageCode && i18nExists(language, messageCode)) message = i18n(language, messageCode, ...args);
		message = message || i18n(language, 'errors.validation.message');
		super(message!);
		this.code = 401;
	}
}
