import { Op } from 'sequelize';
import { sequelize } from '../Database/model';
import Urunler from '../Database/model/urunler';
import DatabaseHelper from '../Database/helpers';
import Kategoriler from '../Database/model/kategoriler';

export default class UrunlerApi {
	static async urunOlustur(data: { urunAdi: string; urunFiyati: number; stokSayisi: number; kategoriId: number }) {
		const urunKontrol = await UrunlerApi.urunBul(data.urunAdi);
		if (urunKontrol.length > 0) return false;
		const transaction = await sequelize.transaction();
		await Urunler.create(
			{
				urunAdi: data.urunAdi,
				urunFiyati: data.urunFiyati,
				stokSayisi: data.stokSayisi,
				kategorilerId: data.kategoriId,
			},
			{ transaction },
		);
		await transaction.commit();
		return true;
	}

	static async urunBul(urunAdi: string) {
		const urunler = await Urunler.findAll({
			where: { urunAdi: { [Op.like]: `%${urunAdi.toLocaleLowerCase()}%` } },
			include: [{ model: Kategoriler, as: 'kategoriler' }],
		});
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(urunler);
	}

	static async tumUrunleriBul() {
		const rows = await Urunler.findAll({
			include: [{ model: Kategoriler, as: 'kategoriler' }],
		});
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(rows);
	}

	static async urunSil(id: number) {
		await Urunler.destroy({ where: { id } });
	}

	static async findById(id: number) {
		const urun = await Urunler.findByPk(id);
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(urun);
	}

	static async urunDuzenle(
		id: number,
		data: { urunAdi: string; urunFiyati: number; stokSayisi: number; kategorilerId: number },
	) {
		const urun = await Urunler.findByPk(id);
		if (urun != null) return false;
		urun!.update(data);
		return true;
	}
}
