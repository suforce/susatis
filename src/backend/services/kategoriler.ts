import { Op } from 'sequelize';
import { sequelize } from '../Database/model';
import DatabaseHelper from '../Database/helpers';
import Kategoriler from '../Database/model/kategoriler';

export default class KategorilerApi {
	static async kategoriOlustur(kategoriAdi: string) {
		const kategoriKontrol = await KategorilerApi.kategoriBul(kategoriAdi);
		if (kategoriKontrol.count > 0) return false;
		const transaction = await sequelize.transaction();
		await Kategoriler.create({ kategoriAdi: kategoriAdi }, { transaction });
		await transaction.commit();
		return true;
	}

	static async kategoriSil(id: number) {
		await Kategoriler.destroy({ where: { id } });
	}

	static async findById(id: number) {
		const kategori = await Kategoriler.findByPk(id);
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(kategori);
	}

	static async kategoriBul(kategoriAdi: string) {
		const kategoriler = await Kategoriler.findAll({
			where: { kategoriAdi: { [Op.like]: `%${kategoriAdi.toLocaleLowerCase()}%` } },
		});
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(kategoriler);
	}

	static async tumKategorileriBul() {
		const rows = await Kategoriler.findAll();
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(rows);
	}

	static async kategoriDuzenle(id: number, kategoriAdi: string) {
		const kategori = await Kategoriler.findByPk(id);
		if (kategori != null) return false;
		else await kategori!.update({ kategoriAdi });
		return true;
	}
}
