import moment from 'moment';
import { Op } from 'sequelize';
import { sequelize } from '../Database/model';
import Urunler from '../Database/model/urunler';
import DatabaseHelper from '../Database/helpers';
import Satislar from '../Database/model/satislar';
import Kategoriler from '../Database/model/kategoriler';

export default class SatislarApi {
	static async satisYap(data: { urunAdedi: number; satisFiyati: number; satisTarihi: Date; urunlerId: number }) {
		const transaction = await sequelize.transaction();
		await Satislar.create(data);
		await transaction.commit();
	}

	/**
	 *
	 * @param tarih dd.mm.yyyy
	 */
	static async satisiBul(urunId: number, tarih: string) {
		const satislar = await Satislar.findAll({
			where: {
				UrunlerId: urunId,
				satisTarihi: { [Op.lte]: moment(tarih, 'DD.MM.YYYY').startOf('day').toDate() },
				[Op.and]: { satisTarihi: { [Op.gte]: moment(tarih).endOf('day') } },
			},
			include: [{ model: Urunler, as: 'urunler', include: [{ model: Kategoriler, as: 'kategoriler' }] }],
		});
		return DatabaseHelper._fillWithRelationsAndFilesForRows(satislar);
	}

	static async tumSatislariBul() {
		const rows = await Satislar.findAll({
			include: [{ model: Urunler, as: 'urunler', include: [{ model: Kategoriler, as: 'kategoriler' }] }],
		});
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(rows);
	}

	static async satisBul(id: number) {
		const satis = await Satislar.findByPk(id);
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(satis);
	}
}
