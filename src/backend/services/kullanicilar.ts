import { Op } from 'sequelize';
import { sequelize } from '../Database/model';
import DatabaseHelper from '../Database/helpers';
import Kullanicilar from '../Database/model/kullanicilar';

export default class KullanicilarApi {

	static async kullaniciOlustur(data: { kullaniciAdi: string; email: string; sifre: string; adSoyad: string }) {
		const kullaniciKontrol = await KullanicilarApi.kullaniciBul(data.kullaniciAdi, data.email);
		if (kullaniciKontrol.length > 0) return false;
		const transaction = await sequelize.transaction();
		await Kullanicilar.create(
			{
				kullaniciAdi: data.kullaniciAdi,
				sifre: data.sifre,
				email: data.email,
				adSoyad: data.adSoyad,
			},
			{ transaction },
		);
		await transaction.commit();
		return true;
	}

	static async findById(id: number) {
		const kullanici = await Kullanicilar.findByPk(id);
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(kullanici);
	}

	static async kullaniciBul(kullaniciAdi: string, email: string) {
		const kullanicilar = await Kullanicilar.findAll({
			where: { [Op.or]: { kullaniciAdi, email } },
		});
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(kullanicilar);
	}

	static async tumKullanicilariBul() {
		const rows = await Kullanicilar.findAll();
		return await DatabaseHelper._fillWithRelationsAndFilesForRows(rows);
	}

	static async kullaniciSil(id: number) {
		await Kullanicilar.destroy({ where: { id } });
	}

	static async kullaniciDuzenle(
		id: number,
		data: { kullaniciAdi: string; adSoyad: string; email: string; sifre: string },
	) {
		const kullanici = await Kullanicilar.findByPk(id);
		if (kullanici != null) return false;
		await kullanici!.update({
			kullaniciAdi: data.kullaniciAdi,
			adSoyad: data.adSoyad,
			email: data.email,
			sifre: data.sifre,
		});
		return true;
	}

	static async girisYap(kullaniciAdi: string, sifre: string) {
		let kullanici = await Kullanicilar.findOne({
			where: { [Op.or]: { kullaniciAdi, email: kullaniciAdi }, [Op.and]: { sifre } },
		});
		if (kullanici != null)
			return { giris: true, kullanici: await DatabaseHelper._fillWithRelationsAndFilesForRows(kullanici) };
		return { giris: false };
	}
}
