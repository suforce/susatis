import path from 'path';
import express from 'express';
import { IpcServer } from 'ipc-express';
import { BrowserWindow, screen, app, ipcMain } from 'electron';
const pug = require('electron-pug');

const ipc = new IpcServer(ipcMain);
const expressApp = express();

let win: BrowserWindow;
async function createWindow() {
	win = new BrowserWindow({
		width: screen.getPrimaryDisplay().workAreaSize.width,
		height: screen.getPrimaryDisplay().workAreaSize.height,
		resizable: false,
		webPreferences: {
			nodeIntegration: true,
			preload: path.join(__dirname, 'frontend', 'assets', 'js', 'preload', 'preload.js'),
		},
	});
	
	pug({ pretty: true });
	win.loadFile(path.join(__dirname, 'frontend', 'giris.pug'));
}
app.whenReady().then(async () => {
	await createWindow();
	app.on('activate', async function () {
		if (BrowserWindow.getAllWindows().length == 0) await createWindow();
	});
});
app.on('window-all-closed', function () {
	if (process.platform != 'darwin') app.quit();
});

ipc.listen(expressApp);

export default expressApp;